# SALE ORDER API

## Authentication

Create SuperUser With Role API ACCESS and give the related methods permissions to the user. Get the generated token for authentication



## Get sale order summary

### Request
```http
GET /order/index HTTP/1.1
Host: integration.mini7.bemacashstage.com.br
Content-Type: application/json
Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJFTUFJTCI6ImZlcm5hbmRvLnplZmVyaTFub0B0b3R2cy5jb20uYnIiLCJQQVNTV09SRCI6IjY2NTdhNjZmMjQxOTM1OGYyMjY3Yzc3ODhhNmRhYmI4In0.FbpgkdIrUQqJjspkoqVD69MeB9pdMtjVdqe99BC9TXg
order_id: 'sale-order-guid'
```

### Response
```json
{
    "items": [
        {
            "description": "pão ",
            "price": 3.68,
            "qty": "13.000",
            "soi_discount": 0,
            "price_type": "UNIT_PRICE",
            "final_gross_price": "0.00",
            "final_discount": "0.00",
            "discountable": "1",
            "extra_cost": null,
            "total_price": 47.84,
            "modifiers": [
                    {
                        "soia_id": "4d6b52e9-7ed7-45db-a29b-bb30b5b0df25",
                        "extra_cost": "3.38",
                        "name": "huhu",
                        "product_name": "pastel",
                        "type": "ADDON"
                    }
            ]
        }
    ],
    "sale_order": {
       "sub_total": "17.18",
       "discount_price": "0.00",
       "tips": "0.00",
       "so_freight": "0.00",
       "so_total": "17.18",
       "so_order_status": "ACTIVE",
       "so_shop_id": "3765",
       "so_nfce_key": null
    },
    "payment": [],
    "merchant": {
        "company_name": "Beraldo RJ LTDA",
        "trade_name": "Beraldo RJ",
        "phone": "22222333333",
        "cnpj": "82373077000171",
        "street": "Rua Mare Alta",
        "complementary": null,
        "city": null,
        "state": null
    }
}
```

## Add payment transaction

### Request
```http
POST /order/index HTTP/1.1
Host: integration.mini7.bemacashstage.com.br
Content-Type: application/json
Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJFTUFJTCI6ImZlcm5hbmRvLnplZmVyaTFub0B0b3R2cy5jb20uYnIiLCJQQVNTV09SRCI6IjY2NTdhNjZmMjQxOTM1OGYyMjY3Yzc3ODhhNmRhYmI4In0.FbpgkdIrUQqJjspkoqVD69MeB9pdMtjVdqe99BC9TXg

{
  "order_id" : "f97161b0-0dd2-477f-9ad2-ab79284f0546",
  "id" : "Payment Id (GUID)",
  "amount" : 2,  
  "card_name" : "VISA",
  "receipt" : "RECEIPT",
  "acquirer_id" : "Operator ID",
  "plots" : "Plots",
  "authorization_number" : "Auth Number",
  "acquirer_number" : "stone ID (Sitef)",
  "gateway" : "PAX_CREDIT",     // all availables gateways
  "payment_from" : "1"  ,// 1 = Ja paguei | 0 = bemacash
  "voucher_type" : "FOOD" // if gateway == PAX_VOUCHER, voucher type is needed (FOOD,MEAL)
}	

```

### Response
   "success or failed" : 
